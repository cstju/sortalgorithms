package sort;
import java.util.Stack;
public class QuickSortIteration {
	public int partition(int[] list,int start,int end){
		int baseNum = list[start];//选择第一个元素作为基准
		int left = start;
		int right = end;
		int temp;
		while(left<right){
			//如果基准数小于list[right]则right前移
			while(left<right&&list[right]>=baseNum){
				right--;
			}
			//如果基准数大于list[left]则left后移
			while(left<right&&list[left]<=baseNum){
				left++;
			}
			//此时left指向的值大于baseNum
			//right指向的值小于baseNum
			//交换这两个值得位置
			temp = list[right];
			list[right] = list[left];
			list[left] = temp;
		}
		//将baseNum和left交换
		//此时left指向数字是最后一个小于baseNum的数字
		temp = list[left];
		list[left] = list[start];
		list[start] = temp;
		return left;
	}
	public int[] sort(int[] disorderList){
		//复制disorderList
		/*
		public static native void arraycopy(Object src,  int  srcPos,  
                Object dest, int destPos,  
                int length);  
		参数：  
		src - 源数组。  
		srcPos - 源数组中的起始位置。  
		dest - 目标数组。  
		destPos - 目标数据中的起始位置。  
		length - 要复制的数组元素的数量。  
		*/
		int[] sortList = new int[disorderList.length];
		System.arraycopy(disorderList,0,sortList,0,disorderList.length);
		if(disorderList==null || disorderList.length==1){
			return disorderList;
		}
		Stack<Integer> index = new Stack<Integer>();
		int listLen = disorderList.length;
		int start = 0;
		int end = listLen-1;
		index.push(start);
		index.push(end);
		while(!index.empty()){
			//其实就是用栈保存每一个待排序子串的首尾元素下标，
			//下一次while循环时取出这个范围，对这段子序列进行partition操作
			int right = index.pop();
			int left = index.pop();
			if(right<=left) continue;
			int mid = partition(sortList,left,right);
			if(left<mid-1){
				//对左子序列排序
				index.push(left);
				index.push(mid-1);
			}
			if(mid+1<right){
				index.push(mid+1);
				index.push(right);
			}
		}
		return sortList;
	}
	
	public static void main(String []args){
		QuickSortIteration test = new QuickSortIteration();
		//int[] testList = test.scanInput();
		int[] testList = {49,38,65,97,76,13,27,49,17,34,12,64,5,4,62,99,98,54,56,17,18,23,34,15,35,25,53,51};
		int[] sortList = test.sort(testList);
		System.out.println("排序之前");
		for(int i=0;i<testList.length;i++){
			System.out.print(testList[i]+" ");
		}
		System.out.println(" ");
		System.out.println("排序之后");
		for(int i=0;i<sortList.length;i++){
			System.out.print(sortList[i]+" ");
		}
	}

}
