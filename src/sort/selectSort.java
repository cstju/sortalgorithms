package sort;
/**
 * 选择排序
 * 在要排序的一组数中，选出最小的一个数与第一个位置的数交换；
 * 然后在剩下的数当中再找最小的与第二个位置的数交换，
 * 如此循环到倒数第二个数和最后一个数比较为止。
 * @author 3dlabuser
 *最好:O(n^2);平均:O(n^2);最坏:O(n^2);辅助空间:O(1);稳定性:稳定
 */
public class selectSort {
	public int[] selectsort(int [] disorderList){
		int listLen = disorderList.length;
		int minIndex;
		int temp;
		for(int i=0;i<listLen;i++){
			temp = disorderList[i];
			minIndex = i;
			for(int j=i;j<listLen;j++){
				if(disorderList[minIndex]>disorderList[j]){
					minIndex=j;
				}
			}
			disorderList[i] = disorderList[minIndex];
			disorderList[minIndex] = temp;
		}
		return disorderList;
	}
	public static void main(String []args){
		selectSort test = new selectSort();
		//int[] testList = test.scanInput();
		int[] testList = {49,38,65,97,76,13,27,49,78,34,12,64,5,4,62,99,98,54,56,17,18,23,34,15,35,25,53,51};
		int[] sortList = test.selectsort(testList);
		for(int i=0;i<sortList.length;i++){
			System.out.print(sortList[i]+" ");
		}
	}

}
