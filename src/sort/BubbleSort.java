package sort;
/**
 * 冒泡排序
 * 在要排序的一组数中，对当前还未排好序的范围内的全部数，
 * 自下而上对相邻的两个数依次进行比较和调整，
 * 让较大的数往下沉，较小的往上冒。
 * 即：每当两相邻的数比较后发现它们的排序与排序要求相反时，就将它们互换。
 * @author 3dlabuser
 *最好:O(n);平均:O(n^2);最坏:O(n^2);辅助空间:O(1);稳定性:稳定
 */
public class BubbleSort {
	public int[] bubblesort1(int [] disorderList){
		int listLen = disorderList.length;
		int temp;
		for (int i=0;i<listLen;i++){
			//两种循环都可以，但是第二种更符合冒泡排序的本意
			//for(int j=1;j<listLen-i;j++){
			for(int j=listLen-1;j>i;j--){
				//从下往上两两进行比较
				//将大的移到下面，小的移到上面
				if(disorderList[j-1]>disorderList[j]){
					temp = disorderList[j-1];
					disorderList[j-1] = disorderList[j];
					disorderList[j] = temp;
					
				}
			}
		}
		return disorderList;
	}
	//优化1：如果某一次遍历没有数据交换，说明已经排序完成
	//不再进行循环遍历
	public int[] bubblesort2(int [] disorderList){
		int listLen = disorderList.length;
		int temp;
		boolean flag = true;
		for (int i=0;i<listLen&&flag;i++){
			flag = false;//初始化标签为false
			for(int j=listLen-1;j>i;j--){
				//从下往上两两进行比较
				//将大的移到下面，小的移到上面
				if(disorderList[j-1]>disorderList[j]){
					temp = disorderList[j-1];
					disorderList[j-1] = disorderList[j];
					disorderList[j] = temp;
					//有数据交换就将标签设为true
					flag = true;
				}
			}
		}
		return disorderList;
	}
	//优化2：记录最后发生数据交换的位置，这个数字之后的数据是已经有序的
	//根据这个数字可以确定下次循环的范围
	public int[] bubblesort3(int [] disorderList){
		int listLen = disorderList.length;
		int temp;
		int k=listLen-1;
		int flag=listLen-1;//记录最后发生数据交换的位置
		for (int i=0;i<listLen;i++){
			////将最后一次交换的位置给flag
			flag = k;
			for(int j=1;j<=flag;j++){
				//从下往上两两进行比较
				//将大的移到下面，小的移到上面
				if(disorderList[j-1]>disorderList[j]){
					temp = disorderList[j-1];
					disorderList[j-1] = disorderList[j];
					disorderList[j] = temp;
					//将交换的位置给k
					k=j;
				}
			}
		}
		return disorderList;
	}
	
	public static void main(String []args){
		BubbleSort test = new BubbleSort();
		//int[] testList = test.scanInput();
		int[] testList = {49,38,65,97,76,13,27,49,78,34,12,64,5,4,62,99,98,54,56,17,18,23,34,15,35,25,53,51};
		int[] sortList = test.bubblesort1(testList);
		for(int i=0;i<sortList.length;i++){
			System.out.print(sortList[i]+" ");
		}
	}

}
