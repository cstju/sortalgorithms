package sort;
/**
 * 归并排序
 * 归并排序是采用分治法的一个非常典型的应用。
 * 归并排序的思想就是先递归分解数组，再合并数组。
 * 先考虑合并两个有序数组，基本思路是比较两个数组的最前面的数，谁小就先取谁，
 * 取了后相应的指针就往后移一位。然后再比较，
 * 直至一个数组为空，最后把另一个数组的剩余部分复制过来即可。
 * 
 * 再考虑递归分解，基本思路是将数组分解成left和right，
 * 如果这两个数组内部数据是有序的，
 * 那么就可以用上面合并数组的方法将这两个数组合并排序。
 * 如何让这两个数组内部是有序的？可以再二分，
 * 直至分解出的小组只含有一个元素时为止，此时认为该小组内部已有序。
 * 然后合并排序相邻二个小组即可。
 * @author 3dlabuser
 * 最好:O(n);平均:O(nlogn);最坏:O(nlogn);辅助空间:O(n);稳定性:稳定
 */
import java.util.Arrays;
public class MergingSort {
	//使用递归方法
	public int[] sort(int [] disorderList){
		int[] orderList = new int [disorderList.length];
		int left = 0;
		int right = disorderList.length-1;
		sortrecursive(disorderList,left,right);
		return disorderList;
	}
	public void merge(int[] data,int left,int center,int right){
		int k = left;
		int[] tmpArr = new int[data.length];
		int leftstart = left;
		int leftend = center;
		int rightstart = center+1;
		int rightend = right;
		while(leftstart<=leftend&&rightstart<=rightend){
			//在左右两个子序中选择较小值依次放到暂存序列中
			if(data[leftstart]<=data[rightstart]){
				tmpArr[k++] = data[leftstart++];
			}else{
				tmpArr[k++] = data[rightstart++];
			}
		}
		while(leftstart<=leftend){
			tmpArr[k++]=data[leftstart++];
		}
		while(rightstart<=rightend){
			tmpArr[k++]=data[rightstart++];
		}
		//将暂存序列中的元素从新拷贝到元序列
		for(k=left;k<=right;k++){
			data[k] = tmpArr[k];
		}
	}
	public void sortrecursive(int [] data,int left,int right){
		if(left>=right) {
			return;
		}
		int center = (left+right)/2;
		sortrecursive(data,left,center);
		sortrecursive(data,center+1,right);
		merge(data,left,center,right);
	}

	//使用迭代方法
	public int[] mergesortiteration(int [] disorderList){
		int listLen = disorderList.length;
		int[] sortList = new int[listLen];
		System.arraycopy(disorderList, 0, sortList, 0, listLen);
		int k = 1;
		while(k<listLen){
			mergeiteration(sortList,k,listLen);
			k *= 2;
		}
		return sortList;
	}
	//MergePass方法负责将数组中的相邻的有k个元素的子序列进行归并
	public void mergeiteration(int[] list,int k,int n){
		int i=0;
		//从前往后,将2个长度为k的子序列合并为1个
		while(i<n-2*k+1){
			merge(list,i,i+k-1,i+2*k-1);
			i += 2*k;
		}
		//这段代码保证了，将那些“落单的”长度不足两个merge的部分和前面merge起来。
		if(i<n-k){
			merge(list,i,i+k-1,n-1);
		}
	}

	public static void main(String [] args){
		MergingSort test = new MergingSort();
		//int[] testList = test.scanInput();
		int[] testList = {49,38,65,97,76,13,27,49,17,34,12,64,5,4,62,99,98,54,56,17,18,23,34,15,35,25,53,51};
		//int[] sortList = test.sort(testList);
		int[] sortList = test.mergesortiteration(testList);
		System.out.println("排序之后");
		for(int i=0;i<sortList.length;i++){
			System.out.print(sortList[i]+" ");
		}
	}

}
