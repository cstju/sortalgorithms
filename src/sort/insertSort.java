package sort;
/**
 * 直接插入排序
 * 在要排序的一组数中，假设前面(n-1)[n>=2] 个数已经是排好顺序的，
 * 现在要把第n个数插到前面的有序数中，使得这n个数
 * 也是排好顺序的。如此反复循环，直到全部排好顺序。
 * @author 3dlabuser
 *
 *最好:O(n);平均:O(n^2);最坏:O(n^2);辅助空间:O(1);稳定性:稳定
 */
import java.util.Scanner;
public class insertSort {
	public int[] scanInput(){
		Scanner scan = new Scanner(System.in);
		System.out.println("请输入待排序元素个数： ");
		int len = scan.nextInt();
		int[] disorderList = new int[len];
		System.out.println("请输入待排序序列： ");
		for(int i=0;i<len;i++){
			disorderList[i] = scan.nextInt();
			System.out.println(disorderList[i]);
		}
		scan.close();
		return disorderList;
	}
	public int[] inserSort(int [] disorderList){
		int[] sortList = new int[disorderList.length];
		System.arraycopy(disorderList,0,sortList,0,disorderList.length);
		int listLen = sortList.length;
		//存储带插入的元素
		int temp;
		for(int i=1;i<listLen;i++){
			int j;
			temp=sortList[i];
			//在i前的序列找到i元素应在位置
			/*循环结束条件:
			 * j<0:说明temp小于之前所有元素
			 * temp>=disorderList[j]
			 * 那应该将temp放到disorderList[j+1]
			*/
			for(j=i-1;j>=0&&temp<sortList[j];j--){
				sortList[j+1]=sortList[j];
			}
			sortList[j+1]=temp;
		}
		return sortList;
	}
	public static void main(String []args){
		insertSort test = new insertSort();
		//int[] testList = test.scanInput();
		int[] testList = {49,38,65,97,76,13,27,49,17,34,12,64,5,4,62,99,98,54,56,17,18,23,34,15,35,25,53,51};
		int[] sortList = test.inserSort(testList);
		System.out.println("排序之前");
		for(int i=0;i<testList.length;i++){
			System.out.print(testList[i]+" ");
		}
		System.out.println(" ");
		System.out.println("排序之后");
		for(int i=0;i<sortList.length;i++){
			System.out.print(sortList[i]+" ");
		}
	}

}
