package sort;
/**
 * 希尔排序（缩减增量排序）
 * 算法先将要排序的一组数按某个增量d（初始为n/2,n为要排序数的个数）分成若干组，
 * 每组中记录的下标相差d.对每组中全部元素进行直接插入排序，
 * 然后再用一个较小的增量（d/2）对它进行分组，
 * 在每组中再进行直接插入排序。
 * 当增量减到1时，进行直接插入排序后，排序完成。
 * @author 3dlabuser
 *最好:O(n^1.3);平均:O(nlogn)~O(n^2)
 *最坏:O(nlogn)~O(n^2);辅助空间:O(1);稳定性:不稳定
 */
public class shellSort {
	public int[] shellsort(int [] disorderList){
		int listLen = disorderList.length;
		int gap = listLen;
		int temp;
		while(gap>1){
			//每一个的增量
			gap = gap/3+1;
			//根据增量步长选出子序列进行插入排序
			for(int i=gap;i<listLen;i++){
				int j;
				temp = disorderList[i];
				for(j = i-gap;j>=0&&temp<disorderList[j];j-=gap){
					disorderList[j+gap] = disorderList[j];
				}
				disorderList[j+gap] = temp;
			}
		}
		return disorderList;
	}
	public static void main(String []args){
		shellSort test = new shellSort();
		//int[] testList = test.scanInput();
		int[] testList = {49,38,65,97,76,13,27,49,78,34,12,64,5,4,62,99,98,54,56,17,18,23,34,15,35,25,53,51};
		int[] sortList = test.shellsort(testList);
		for(int i=0;i<sortList.length;i++){
			System.out.print(sortList[i]+" ");
		}
	}
}
