package sort;
/**
 * 快速排序
 * 从数列中挑出一个元素，称为"基准"，
 * 重新排序数列，所有比基准值小的元素摆放在基准前面，
 * 所有比基准值大的元素摆在基准后面（相同的数可以到任一边）。
 * 在这个分区结束之后，该基准就处于数列的中间位置。
 * 这个称为分区（partition）操作。
 * 递归地（recursively）把小于基准值元素的子数列和大于基准值元素的子数列排序。
 * @author 3dlabuser
 *最好:O(nlogn);平均:O(nlogn);最坏:O(n^2);辅助空间:平均O(logn)最坏O(n);稳定性:不稳定
 */
public class QuickSort {

	public void quickSortRecursive(double [] list,int start,int end){
		if(start>=end) return;
		//每次选取列表的第一个元素作为基准
		double baseNum = list[start];
		int left = start;
		int right = end;
		//double temp;
		while(left<right){
			//如果基准数小于list[right]则right前移
			while(left<right&&list[right]>=baseNum){
				right--;
			}
			//否则将list[right]替换（不是交换）list[left]
			list[left] = list[right];
			//如果基准数大于list[left]则left后移
			while(left<right&&list[left]<=baseNum){
				left++;
			}
			//否则将list[left]替换（不是交换）list[right]
			list[right] = list[left];
		}
		//将baseNum和left交换
		//此时left指向数字是最后一个小于baseNum的数字
		list[left] = baseNum;
		//完成上述操作后，所有小于baseNum的在左侧，大于在右侧
		//递归生成左右子序列
		quickSortRecursive(list,start,left-1);
		quickSortRecursive(list,left+1,end);
	}
	
	public double[] sort(double [] disorderList){
		//复制disorderList
		double[] sortList = new double[disorderList.length];
		System.arraycopy(disorderList,0,sortList,0,disorderList.length);
		int listLen = sortList.length;
		int start = 0;
		int end = listLen-1;
		quickSortRecursive(sortList,start,end);
		return sortList;
	}
	public static void main(String []args){
		QuickSort test = new QuickSort();
		//int[] testList = test.scanInput();
		double[] testList = {49,38,65.5,97,76,13,27,49,17,34,12,64,5,4,62,99,98,54,56,17,18,23,34,15,35,25,53,51};
		double[] sortList = test.sort(testList);
		System.out.println("排序之前");
		for(int i=0;i<testList.length;i++){
			System.out.print(testList[i]+" ");
		}
		System.out.println(" ");
		System.out.println("排序之后");
		for(int i=0;i<sortList.length;i++){
			System.out.print(sortList[i]+" ");
		}
	}

}
