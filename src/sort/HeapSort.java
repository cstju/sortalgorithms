package sort;
/**
 * 堆排序
 * 堆排序在 top K 问题中使用比较频繁。堆排序是采用二叉堆的数据结构来实现的，
 * 虽然实质上还是一维数组。二叉堆是一个近似完全二叉树 。
 * 大顶堆：每一个结点的值都大于等于其左右孩子结点的值
 * 小顶堆：每一个结点的值都小于等于其左右孩子结点的值
 * 将待排序序列构成一个大顶堆，此时整个序列的最大值就是堆顶的根结点。将其与堆数组的
 * 末尾元素交换此时末尾元素就是最大值，然后将剩余的n-1个序列从新构成一个堆，如此反复
 * 便能得到一个有序序列
 * @author 3dlabuser
 * 最好:O(nlogn);平均:O(nlogn);最坏:O(nlogn);辅助空间:O(1);稳定性:不稳定
 */
public class HeapSort {
	public int[] sort(int [] disorderList){
		int listLen = disorderList.length;
		int temp;
		//将待排序序列构建成一个大顶堆
		//从listLen/2-1开始向前循环是因为，
		//根据完全二叉树的定义listLen/2-1之前的都有子节点
		for(int start=listLen/2-1;start>=0;start--){
			maxHeapify(disorderList,start,listLen-1);
		}
		//将大顶堆的根节点与末尾元素交换
		//并再次调整其为大顶堆
		for(int end=listLen-1;end>0;end--){
			temp=disorderList[end];
			disorderList[end]=disorderList[0];
			disorderList[0]=temp;
			maxHeapify(disorderList,0,end-1);
		}
		
		return disorderList;
	}
	public void maxHeapify(int [] list,int start,int end){
		int temp;
		//int child;
		//由完全二叉树的性质可得，当前节点序号是start，
		//其左子节点序号一定是2S+1,右子节点的序号一定是2S+2
		for(int child = 2*start+1;child<=end;child=2*child+1){
			//list[child]<list[child+1]说明左子节点小于右子节点
			//最大堆要找较大值则child++，让child指向右子节点
			if(child<end && list[child]<list[child+1]){
				child++;
			}
			//根节点list[start]大于list[child]
			//说明根节点大于左右子节点，跳出这个循环
			if(list[start]>=list[child]){
				break;
			//否则交换子节点和根节点的值
			//并将child对应的下标给start
			}else{
				temp = list[start];
				list[start] = list[child];
				list[child] = temp;
				start=child;
			}
		}
	}
	public static void main(String []args){
		HeapSort test = new HeapSort();
		//int[] testList = test.scanInput();
		int[] testList = {49,38,65,97,76,13,27,49,78,34,12,64,5,4,62,99,98,54,56,17,18,23,34,15,35,25,53,51};
		int[] sortList = test.sort(testList);
		for(int i=0;i<sortList.length;i++){
			System.out.print(sortList[i]+" ");
		}
	}

}
